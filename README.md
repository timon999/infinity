# Infinity #

Simple Game. Inspired by [Infinite Loop](http://lekevicius.com/projects/infinite-loop/). Built with [Löve](https://love2d.org/).

Using [HUMP](http://hump.readthedocs.org/en/latest/) and [inspect.lua](https://github.com/kikito/inspect.lua).

### How to run this game

Windows:

[Download](https://bitbucket.org/timon999/infinity/downloads/infinity_win.zip), run infinity\_win.exe

Gnu/Linux, Mac OS:

[Download](https://bitbucket.org/timon999/infinity/downloads/infinity.love), follow [this guide](https://love2d.org/wiki/Getting_Started)