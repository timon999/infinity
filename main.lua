--[[
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

--]]


lg = require("levelgen")
levelsolved = require("levelsolved")
p = require("parameters")
inspect = require("lib/inspect")
Timer = require("lib/hump.timer")
Camera = require("lib/hump.camera")
Gamestate = require("lib/hump.gamestate")

LEVEL_X = p.x
LEVEL_Y = LEVEL_X
WINDOW_X = p.w
WINDOW_Y = WINDOW_X
IMG_X = 5
IMG_Y = 5
ROT_SPEED = 0.25

menu = {}
game = {}

function new_level()
  math.randomseed(levelnr)
  level = lg.make_level(LEVEL_X, LEVEL_Y)
  level_rot = twodarray(LEVEL_X, LEVEL_Y, function() return math.random(1,4) end)
  level_rot_an = clone(level_rot)
  cam_pos.y = WINDOW_Y*1.5
  Timer.tween(1, cam_pos, { y = WINDOW_Y/2}, "bounce")
end

function clone(orig)
  -- von http://lua-users.org/wiki/CopyTable
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[clone(orig_key)] = clone(orig_value)
        end
        setmetatable(copy, clone(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function love.load()
  cam_pos = {x = WINDOW_X/2, y = WINDOW_Y/2}
  cam = Camera(cam_pos.x, cam_pos.y)
  love.graphics.setNewFont("assets/Roboto-Light.ttf", 64)
  love.graphics.setColor(255, 209, 128)
  levelnr = love.filesystem.read("levelnr") or 1
  is_solved = false
  solved = {y = -200}
  take_input = true
  love.graphics.setDefaultFilter("nearest", "nearest")
  sprites = {
    cross = love.graphics.newImage("assets/cross.png"),
    curve = love.graphics.newImage("assets/curve.png"),
    endpoint = love.graphics.newImage("assets/end.png"),
    line = love.graphics.newImage("assets/line.png"),
    tcross = love.graphics.newImage("assets/t.png"),
    empty = love.graphics.newImage("assets/empty.png")
  }
  solved_sprite = love.graphics.newImage("assets/solved.png")
  love.graphics.setBackgroundColor(255, 209, 128)
  Gamestate.registerEvents{"draw", "keypressed", "mousepressed", "update"}
  Gamestate.switch(menu)
end

function game:draw()
  cam:attach()
  for x=1,LEVEL_X do
    for y=1,LEVEL_Y do
      love.graphics.draw(sprites[level[x][y]], 
        (x-0.5)*WINDOW_X/LEVEL_X, (y-0.5)*WINDOW_Y/LEVEL_Y, level_rot_an[x][y]*math.pi/2,
        WINDOW_X/LEVEL_X/IMG_X, WINDOW_Y/LEVEL_Y/IMG_Y,
        sprites[level[x][y]]:getWidth()/2, sprites[level[x][y]]:getHeight()/2)
    end
  end
  cam:detach()
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.printf("Level "..levelnr, WINDOW_X/2-200, solved.y, 400,"center")
  love.graphics.setColor(255, 209, 128)
end

function game:update(dt)
  Timer.update(dt)
  cam:lookAt(cam_pos.x, cam_pos.y)
  for y=1,#level_rot do
    for x=1,#level_rot[1] do
      level_rot_an[y][x] = level_rot_an[y][x] + (level_rot[y][x] - level_rot_an[y][x])*ROT_SPEED
    end
  end
  if love.keyboard.isDown("f") then print(1/dt) end
end
function game:keypressed(key, isrepeat)
  print(inspect(level_rot), inspect(level_rot_an))
  print(level_rot, level_rot_an)
end

function game:mousepressed(x, y, button)
  if y == 0 then y = 1 end
  if take_input then
  
  if is_solved then
    new_level()
    is_solved = false
    Timer.tween(0.5, solved, { y = -200 }, "quint")
  end
  local lx, ly = math.ceil(x/(WINDOW_X/LEVEL_X)), math.ceil(y/(WINDOW_Y/LEVEL_Y))
  level_rot[lx][ly] = level_rot[lx][ly] + ((button == "l") and 1 or -1)
  if levelsolved(level, level_rot) then
    is_solved = true
    levelnr = levelnr + 1
    Timer.tween(1, solved, { y = WINDOW_Y/4}, "bounce")
    take_input = false
    Timer.add(0.75, function() take_input = true end)
  end
  
  end
end

function menu:update(dt)
  Timer.update(dt)
end

menutext = { alpha = 255 }
function menu:draw()
  love.graphics.setColor(0, 0, 0, menutext.alpha)
  love.graphics.printf("L∞P", WINDOW_X/2-200, WINDOW_Y/4, 400,"center")
end

function menu:mousepressed(x, y, button)
  if take_input then
    Timer.tween(1, menutext, { alpha = 0 }, "linear")
    Timer.add(1.2, function() take_input = true Gamestate.switch(game) new_level() end)
    take_input = false
  end
end

function love.quit()
  love.filesystem.write("levelnr", tostring(levelnr))
end