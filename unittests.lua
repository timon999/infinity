require "levelsolved"
require "levelgen"
m = require("moontest")

-- levelsolved.shiftarray
m:asserteq(shiftarray({1,2,3,4,5},2), {4,5,1,2,3})
m:asserteq(shiftarray({1,2,3,4,5},7), {4,5,1,2,3})

-- levelsolved.shiftbyone
m:asserteq(shiftbyone({1,2,3,4,5}),{5,1,2,3,4})
m:asserteq(shiftbyone({1}),{1})

-- levelsolved.head
m:asserteq(head({1,2,3,4,5}),{1,2,3,4})
m:asserteq(head({5,4,3,2,1}),{5,4,3,2})

-- levelgen.Set
m:asserteq(Set{"a","b","c"},{a = true, b = true, c = true})

-- levelgen.scnd
m:asserteq(scnd(1,2,3,4), 2)
m:asserteq(scnd({"a","b"}, "c", 1, 2), "c")

-- levelgen.randboolean
m:asserteq(type(randboolean()), "boolean")

-- levelgen.index
m:asserteq(index({"a","b","c"}, "b"), {2})
m:asserteq(index({"a","b","a"}, "a"), {1,3})

-- levelgen.true_elements_in
m:asserteq(true_elements_in{true, false, true, true}, 3)
m:asserteq(true_elements_in{true, "a", false, 5}, 1)

