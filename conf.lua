p = require("parameters")
function love.conf(t)
  t.window.width = p.w
  t.window.height = p.h
  t.console = true
  t.title = "L∞P"
end