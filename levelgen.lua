local inspect = require("lib/inspect")

function Set (list)
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set
end

function scnd(x,y,...)
  return y
end

function randboolean()
  return math.random()>0.5
end

function index(array,value)
  indices = {}
  for k,v in ipairs(array) do
    if v == value then indices[#indices+1] = k end
  end
  return indices
end

function true_elements_in(array)
  return #index(array, true)
end

function twodarray(x,y,value_func)
  value_func = value_func or function() return false end
  local mt = {}          
  for i=1,x do
    mt[i] = {}     
    for j=1,y do
      mt[i][j] = value_func()
    end
  end
  return mt
end

function make_level(x,y)
  vert_lines = twodarray(x-1,y,randboolean)
  hori_lines = twodarray(x,y-1,randboolean)
  level = twodarray(x,y)
  for i=1,x do
    for j=1,y do
      borders = { scnd(pcall(function() return hori_lines[i][j-1] end)), 
                  scnd(pcall(function() return vert_lines[i][j] end)), 
                  scnd(pcall(function() return hori_lines[i][j] end)), 
                  scnd(pcall(function() return vert_lines[i-1][j] end)) }
      for i=1,#borders do
        if Set{"string", "nil"}[type(borders[i])] then borders[i] = false end
      end
      num_borders = true_elements_in(borders)
      if num_borders == 0 then
        level[i][j] = "empty"
      elseif num_borders == 1 then
        level[i][j] = "endpoint"
      elseif num_borders == 3 then
        level[i][j] = "tcross"
      elseif num_borders == 4 then
        level[i][j] = "cross"
      elseif num_borders == 2 then
        indices = index(borders, true)
        if math.abs(indices[1]-indices[2]) == 2 then
          level[i][j] = "line"
        else
          level[i][j] = "curve"
        end
      end
    end
  end
  return level
end

return {
  make_level = make_level,
  twodarray = twodarray
}