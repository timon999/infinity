function levelsolved(level, level_rot)
  local is_solved = true
  for y=1,#level do
    for x=1,#level[1] do
      if not tilevalid(level, level_rot, x, y) then is_solved = false end
    end
  end
  return is_solved
end

function tilevalid(level, level_rot, x, y)
  local is_valid = true
  local x_len, y_len = #level[1], #level
  if not (check_border("left", level, level_rot, x, y) == check_border("right", level, level_rot, x-1, y)) then is_valid = false end
  if not (check_border("right", level, level_rot, x, y) == check_border("left", level, level_rot, x+1, y)) then is_valid = false end
  if not (check_border("up", level, level_rot, x, y) == check_border("down", level, level_rot, x, y-1)) then is_valid = false end
  if not (check_border("down", level, level_rot, x, y) == check_border("up", level, level_rot, x, y+1)) then is_valid = false end
  return is_valid
end

function check_border(dir, level, level_rot, x, y)
  local tiles = {
  curve = { true, true, false, false },
  line = { true, false, true, false },
  tcross = { true, true, true, false },
  empty = { false, false, false, false },
  cross = { true, true, true, true },
  endpoint = { true, false, false, false }
  }
  if x<1 or x>#level[1] or y<1 or y>#level then 
    return false 
  else
    dirs = {up = 1, right = 2, down = 3, left = 4}
    tile = tiles[ level[x][y] ]
    tile = shiftarray( tile, level_rot[x][y] )
    return tile[ dirs[dir] ]
  end
end

function shiftarray(a, n)
  n = n % #a
  for i=1, n do
    a = shiftbyone(a)
  end
  return a
end

function shiftbyone(a)
  local s = {a[#a]}
  for _,v in ipairs(head(a)) do
    table.insert(s, v)
  end
  return s
end

function head(a) 
  local h = {}
  for i=1,#a-1 do
    table.insert(h, a[i])
  end
  return h
end

return levelsolved