local inspect = require("lib/inspect")
local deepcompare = require("lib/deepcompare")

Moontest = {n = 0}

function Moontest:assert(s)
  self.n = self.n + 1
  if not loadstring("return "..s)() then
    print("Failed Test Nr.", self.n, ":", s)
    os.exit()
  else
    print("Test", self.n, "passed")
  end
end

function Moontest:asserteq(x,y)
  self.n = self.n + 1
  if deepcompare(x,y) then
    print("Test", self.n, "passed. Value:",inspect(x))
  else
    print("Failed Test Nr.", self.n, ":", inspect(x), "is not", inspect(y))
    os.exit()
  end
end

function Moontest:assertuneq(x,y)
  self.n = self.n + 1
  if not deepcompare(x,y) then
    print("Test", self.n, "passed. Value:",inspect(x))
  else
    print("Failed Test Nr.", self.n, ":", inspect(x), "is not", inspect(y))
    os.exit()
  end
end

return Moontest